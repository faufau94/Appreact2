import React, { Component } from 'react';

import {
  View,
  Text,
  StatusBar
} from 'react-native';

import {Login} from './Components/Login';
import {styles} from './Components/Style';

export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <StatusBar hidden={true}/>
        <Home />
      </View>
    );
  }
}


