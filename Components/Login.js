import React, { Component } from 'react';

import {
  View,
  Text,
  TextInput,
  TouchableOpacity
} from 'react-native';

import {styles} from './Style';
import {StackNavigator} from 'react-navigation';
import Acceuil from './Home';

export class Login extends Component {


	go(){
		this.props.navigation.navigate('Home')
	}
	render() {
		return ( 
		<View> 
			
			 <View style={styles.block}>
		      <Text style={styles.texte}>
		        ROTTER
		      </Text>
		      <Text style={styles.small_texte}>
		        The best application made for you !
		      </Text>
	         </View>

	         <View style={styles.block_input}>
	         	<TextInput 
	         	style = {styles.input} 
	         	placeholder = "Username..."
	         	placeholderTextColor= "white"
	         	underlineColorAndroid = "transparent"
	         	/>
	         	<TextInput 
	         	style = {styles.input} 
	         	placeholder = "Password..."
	         	placeholderTextColor= "white"
	         	underlineColorAndroid = "transparent"
	         	secureTextEntry={true}
	         	/>
	         </View>

		         <TouchableOpacity
		         onPress = {this.go}
		         >
		         	<View style={styles.touchView}>
		         		<Text style={styles.touchText}>
		         		  Go !
		         		</Text>
		         	</View>
		         </TouchableOpacity>
         </View>
		);
	}
}

export default StackNavigator({
	Home: {
		screen: Accueil
	}
})


