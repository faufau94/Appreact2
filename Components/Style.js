import React from 'react';
import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor: "#52F4FF",
    alignItems: 'center',
    justifyContent: 'center',
  },
  block: {
    marginBottom: 70
  },
  texte: {
    color: "white",
    textAlign: "center",
    fontSize: 40,
  },
  small_texte:{
    color: "white",
    fontSize: 20,
    textAlign: "center"
  },
  block_input:{
    marginTop: 80,
    marginBottom: 80,
    alignItems: 'center'
  },
  input: {
    backgroundColor: "#52F4FF",
    color: "white",
    fontSize: 20,
    marginBottom: 30,
    padding: 10,
    paddingLeft: 10,
    width: 240,
    borderWidth: 2,
    borderRadius: 14,
    borderColor: "white",
  },
  touchView: {
    marginLeft: 120,
    height: 70,
    width: 70,
    borderRadius: 35,
    backgroundColor: "#3498db",
    paddingTop: 15.5,
  },
  touchText: {
    color: "white",
    fontSize: 25,
    marginBottom: 90,
    textAlign: 'center' 
  }
});